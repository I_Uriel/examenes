﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rest_Perros.Models
{
    [MetadataType(typeof(Perros.MetaData))]
    public partial class Perros
    {
        sealed class MetaData { 
        
            [Key]
            public int P_id;

            [Required(ErrorMessage = "Ingresa el nombre del Perro")]
            public string Nombre;

            [Required(ErrorMessage = "Ingresa la raza")]
            public string Raza;

            [Required(ErrorMessage = "Ingresa la vacuna")]
            public string Vacunas;

            [Required(ErrorMessage = "Ingresa el sexo")]
            public string Sexo;

            [Required]
            [Range(1, 22, ErrorMessage = "Edad entre 1 y 22")]
            public Nullable<int> Edad;



        }
    }
}