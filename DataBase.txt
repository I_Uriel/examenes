CREATE DATABASE VET;
USE VET;

CREATE TABLE Perros(
	P_id int not null primary key identity(1,1),
	Nombre nvarchar(25),
	Raza nvarchar(25),
	Vacunas nvarchar(25),
	Sexo nvarchar(10),
	Edad int
);

insert into Perros (Nombre,Raza,Vacunas,Sexo,Edad)
values
('Frida', 'Pitbull blue', 'Parmovirus', 'Hembra', '2');

select* from Perros;