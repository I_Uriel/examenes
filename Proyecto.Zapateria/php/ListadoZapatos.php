<?php
//incluir archivo de conexion
    include("conexion.php");

//crear la consulta para listar datos
    $consulta = "SELECT
                    id_zap,
                    modelo,
                    precio,
                    marca,
                    talla
                  FROM zapatos";

     $ejecuta = $conexion -> query($consulta) or die("Error al consultar lista de zapatos <br> :" . $conexion -> error);

?>
<!-- Encabezado de la taabla-->
<table id="lista_zapatoss">
	<tr>
		<th>ID</th>
		<th>Modelo</th>
		<th>Precio</th>
		<th>Marca</th>
		<th>Talla</th>
    <th colspan="2">Edicion</th>
	</tr>
<?php

    while( $arreglo_resultados = $ejecuta -> fetch_row() ){
        echo '<tr>';
        echo '<td>' . $arreglo_resultados[0] . '</td>';
        echo '<td>' . $arreglo_resultados[1] . '</td>';
        echo '<td>' . $arreglo_resultados[2] . '</td>';
        echo '<td>' . $arreglo_resultados[3] . '</td>';
        echo '<td>' . $arreglo_resultados[4] . '</td>';
        //boton para editar
        echo '<td> <button type="button" onclick="formZapato('.$arreglo_resultados[0].');">';
        echo 'Editar</button></td>';
        	//boton para eliminar
        echo '<td> <button type="button" onclick="eliminarZapato('.$arreglo_resultados[0].');">';
        echo 'Eliminar</button></td>';
        echo '</tr>';
    }
?>

</table>
